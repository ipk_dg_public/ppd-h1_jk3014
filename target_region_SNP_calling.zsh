# 20Mb is the interval for each Bed file

# for Ppd-H1, chr2H.part2.bed include chr2H regions 20000001~40000000
cd /filer-dg/agruppen/dg4/guoy/ancient/s3.variant/s2.call/chr2H;/opt/Bio/bcftools/1.9/bin/bcftools mpileup --threads 4 -a DP,AD -q 20 -Q 20 --excl-flags 3332 -R ../../s1.index/chr2H.part2.bed -f /filer-dg/agruppen/dg5/guoy/config/genome/barley/v1/fa/refer.fa -b ../bam.list 2>chr2H.part2.err \
 | /opt/Bio/bcftools/1.9/bin/bcftools call --threads 4 -m -O z -o part2.vcf.gz >chr2H.part2.out 2>>chr2H.part2.err;echo finish >>chr2H.part2.out

# for HvCEN, chr2H.part27.bed include chr2H regions 520000001~540000000 
cd /filer-dg/agruppen/dg4/guoy/ancient/s3.variant/s2.call/chr2H;/opt/Bio/bcftools/1.9/bin/bcftools mpileup --threads 4 -a DP,AD -q 20 -Q 20 --excl-flags 3332 -R ../../s1.index/chr2H.part27.bed -f /filer-dg/agruppen/dg5/guoy/config/genome/barley/v1/fa/refer.fa -b ../bam.list 2>chr2H.part27.err \
 | /opt/Bio/bcftools/1.9/bin/bcftools call --threads 4 -m -O z -o part27.vcf.gz >chr2H.part27.out 2>>chr2H.part27.err;echo finish >>chr2H.part27.out
