#!/bin/zsh

cd /filer-dg/agruppen/dg4/guoy/ancient/s2.align/s1.align/JK3014

/opt/Bio/bwa/0.7.17/bin/bwa mem -R '@RG\tID:JK3014\tLB:JK3014\tPL:ILLUMINA\tPU:JK3014\tSM:JK3014' -M -t 30 /filer-dg/agruppen/dg5/guoy/config/genome/barley/v1/fa/refer ../../../s1.clean_data/s2_1.merge/JK3014/1.fq.gz 2>bwa.err \
 | /opt/Bio/samtools/1.9/bin/samtools view -Su /dev/stdin 2>samtools.err \
 | /opt/Bio/novocraft/V3.06.05/bin/novosort -c 4 --md --ise /dev/stdin 2>novosort.err \
 | /opt/Bio/samtools/1.9/bin/samtools view -t /filer-dg/agruppen/dg5/guoy/config/genome/barley/v1/fa/refer.fa.fai -b -o sort.bam /dev/stdin

/opt/Bio/samtools/1.9/bin/samtools index -c sort.bam

echo finish >align.out
