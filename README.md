This repository holds UNIX shell scripts for read alignment and SNP calling of the ancient barley sample JK3014.

Results are described in https://doi.org/10.1101/2020.07.02.185488.